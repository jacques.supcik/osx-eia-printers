#!/bin/bash

SERVER="hefrprint"
MODEL="Library/Printers/PPDs/Contents/Resources/KONICAMINOLTAC754e.gz KONICA MINOLTA C754e PS"

PRINTERS="
  MF_B-C10
  MF_B-C20
  MF_C-D10
  MF_C-D20
  MF_D30C
  FollowMe
"

for p in $PRINTERS; do
    lpstat -a "${p}_PS" 1>/dev/null 2>/dev/null
    if [ $? -eq 0 ]; then
        echo "Printer ${p}_PS already installed"
    else
        echo "Adding ${p}_PS"
        sudo lpadmin \
            -p "${p}_PS" \
            -m "$MODEL" \
            -P "/etc/cups/ppd/c754e-3.2.0.ppd" \
            -v "smb://${SERVER}/${p}_PS" \
            -L "HEFR ${p}" \
            -E
    fi
done

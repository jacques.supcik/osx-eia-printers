# Installation des imprimantes pour OSX 10.9

## Pour la c754e

1. Installez le dernier driver, disponible sur le site de Konica/Minolta ou en copie [ici](../raw/master/software/bizhub_C754e_109.dmg)
2. Télécharger le [fichier PPD](../raw/master/software/c754e-3.2.0.ppd) et copiez-le avec le nom "/etc/cups/ppd/754e-3.2.0.ppd". (N.B. On peut aussi se passer de cette phase, mais ça compliquerait la suite...)
3. Exécutez le [script d'installation](../raw/master/eia_printers.sh)

## Pour la c652

Cette imprimante n'est plus supporté par ce script.

